﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace WindowsFormsApp20
{
	public partial class Form1 : Form
	{
		public static List<(Keys Hotkey, string DestinationPath)> Hotkeys;
		public static string CurrentDirectory;

		public static bool TryAddHotkey(string key, string path)
		{
			if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(path))
				return false;

			if (!Directory.Exists(path))
			{
				MessageBox.Show($"Destination folder seems to not exist or is invalid");
				return false;
			}

			bool validKey = 
				Enum.TryParse(key, out Keys parsedKey) 
				&& !Hotkeys.Any(keys => keys.Hotkey == parsedKey)
				&& !Hotkeys.Any(keys => keys.DestinationPath == path);

			if (!validKey)
			{
				if (!Enum.TryParse<Keys>(key, out _)) {
					MessageBox.Show($"Failed parsing '{key}' to an actual Keys object!");
					return false;
				}
					
				if (Hotkeys.Any(keys => keys.Hotkey == parsedKey)) { 
					MessageBox.Show("Specified hotkey already exists in list of hotkeys!");
					return false;
				}

				if (Hotkeys.Any(keys => keys.DestinationPath == path)) 
					MessageBox.Show("Specified folder destination already exists in list of hotkeys!");

				return false;
			}
				
			Hotkeys.Add((parsedKey, path));
			return true;
		}
		
		public Form1()
		{
			InitializeComponent();
			Hotkeys = new List<(Keys, string)>();
			Text = "Please select a folder to start sorting ...";
		}

		private void groupBox1_Enter(object sender, EventArgs e)
		{

		}

		private void Form1_KeyUp(object sender, KeyEventArgs e)
		{
			if (textBox1.Focused)
			{
				textBox1.Text = e.KeyCode.ToString();
			} else
			{
				var hotkeyPressed = Hotkeys.FirstOrDefault(keys => keys.Hotkey == e.KeyCode);
				if (hotkeyPressed == default) return;

				string newPath = $"{hotkeyPressed.DestinationPath}\\{Path.GetFileName(pictureBox1.ImageLocation)}";
				bool fileExists = File.Exists(newPath);
				if (fileExists)
				{
					MessageBox.Show($"File with name '{Path.GetFileName(pictureBox1.ImageLocation)}' already exists in folder '{hotkeyPressed.Item2}'");
					return;
				}

				if (string.IsNullOrEmpty(pictureBox1.ImageLocation)
					|| !File.Exists(pictureBox1.ImageLocation))
						return;
				try
				{
					File.Move(pictureBox1.ImageLocation, newPath);

					int imageCount = Directory.GetFiles(CurrentDirectory, "*.*").Where(filePath => Regex.IsMatch(filePath, @".jpg|.png|.jpeg|.bmp|.gif$")).Count();
					toolStripStatusLabel1.Text = $"Images Left: {imageCount}";
					if (imageCount > 0)
					{
						pictureBox1.ImageLocation = Directory.GetFiles(CurrentDirectory, "*.*").Where(filePath => Regex.IsMatch(filePath, @".jpg|.png|.jpeg|.bmp|.gif$")).ToArray()[0];
						Text = $"Sorting directory '{CurrentDirectory}' ({Path.GetExtension(pictureBox1.ImageLocation)})";
					}
					else
					{
						pictureBox1.ImageLocation = "";
						pictureBox1.Refresh();
						Text = "Please select a folder to start sorting ...";
						MessageBox.Show("No images left to sort!");
					}
				} catch (Exception ex)
				{
					Debug.WriteLine($"Exception of type '{ex.InnerException.GetType()}' caught with message '{ex.Message}'");
				}
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			bool addResult = TryAddHotkey(textBox1.Text, textBox2.Text);
			if (addResult)
			{
				int addedRowIdx = dataGridView1.Rows.Add(textBox1.Text, textBox2.Text);
				if (addedRowIdx == -1)
					Debug.WriteLine($"[WARNING] Method 'dataGridView1.Rows.Add()' method returned -1 with parameters [Hotkey: '{textBox1.Text}', DestinationPath: '{textBox2.Text}']");

				textBox1.Text = "Hotkey";
				textBox2.Text = "Destination Path";
			}	
		}

		private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
			using var fbd = new FolderBrowserDialog
			{
				Description = "Select the root folder containing the images you want to sort",
				UseDescriptionForTitle = true
			};

			if (fbd.ShowDialog() == DialogResult.OK)
			{
				CurrentDirectory = fbd.SelectedPath;
				int imageCount = Directory.GetFiles(fbd.SelectedPath, "*.*").Where(filePath => Regex.IsMatch(filePath, @".jpg|.png|.jpeg|.bmp|.gif$")).Count();

				toolStripStatusLabel1.Text = $"Images Left: {imageCount}";

				if (imageCount > 0)
				{
					pictureBox1.ImageLocation = Directory.GetFiles(fbd.SelectedPath, "*.*").Where(filePath => Regex.IsMatch(filePath, @".jpg|.png|.jpeg|.bmp|.gif$")).ToArray()[0];
					Text = $"Sorting directory '{fbd.SelectedPath}' ({Path.GetExtension(pictureBox1.ImageLocation)})";

					int notSupportedImageCount = Directory.GetFiles(CurrentDirectory, "*.*").Where(path => Path.GetExtension(path).Contains("webp")).Count();
					if (notSupportedImageCount > 0)
						MessageBox.Show($"Detected {notSupportedImageCount} image files with .webp exception that is not supported in folder '{CurrentDirectory}'");
				}
				else
				{
					Text = "Please select a folder to start sorting ...";
					MessageBox.Show("No images found in selected folder!");
				}
			}
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			
		}
	}
}
